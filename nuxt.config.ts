// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  app: {
    baseURL: "/cv",
  },

  modules: [
    "nuxt-lodash",
    "@nuxt/content",
    "@nuxtjs/tailwindcss",
    "nuxt-lucide-icons",
    "@nuxt/image",
    "@nuxtjs/i18n",
  ],

  typescript: {
    //typeCheck: true,
  },

  i18n: {
    strategy: 'prefix_except_default',
    locales: ['en', 'fr'],
    defaultLocale: 'en',
  },

  compatibilityDate: "2024-09-06",
})
