# Curriculum Vitae

This is the source code for my CV. Compiled version can be found on [my website](https://jip.rezal.cc/cv)

It is written in vuejs using Nuxt Content, TailwindCss, DaisyUI and LucideIcons.

Most of the source code is pretty standard: content is written in Markdown in the `content` directory, and uses custom components written in the `components/content` directory for layout.

The `content` directory is expected to contain a sub-directory per language.

What might be of interest to others are the `ComponentContentDoc` and `ComponentContentDocNav` components. The latter one is only meant to be used for the former one.

The `ComponentContentDoc` component has two uses:

If the path is a Markdown file, it can wrap the content in a component, with props taken from the front matter.

If the path is a directory and has a `_dir.yml` file, it can recursively render the content of the directory, optionaly wrapped in a component. Content inside the directory can either be rendered as a list of child components, or can be used as templates for the slots of the wrapping component.

This allows splitting the document into several Markdown file with minimal boilerplate.

This component uses vuejs dynamic components, named dynamic scoped templates, and the `$slots` and `$props` variables under the hood.

## TODO

- Finish Typescript declarations and fix errors
- Write documentation for the few generic components
- Follow-up a [Nuxt Content bug](https://github.com/nuxt/content/issues/2660)
- Write a bug report to vuejs to understand why the following snippet is not working
```
<template v-if="tlist" v-for="t in tlist" :key="t.name" #[t.name]>
  {{ t.content }}
</template>
```
