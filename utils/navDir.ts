export default function(path: String) {
  return path + (path.endsWith('/') ? '' : '/')  + '_dir'
}
