---
title: Expériences informatiques
component: LayoutSection
---

::TimelineList

  ::TimelineEntry
  ---
  title: Auto-hébergement, besoins personnels
  startTime: 2005
  endTime: Présent
  first: true
  icon: square-terminal
  ---

  Auto-hébergement de mon infrastructure personnelle avec **Podman/Docker** et **Ansible**.

  **Traefik**, **Knot** (DNS), **Stalwart** (SMTP/IMAP/JMAP), **Nextcloud** et diverses appli **PHP/RoR**
  ::


  ::TimelineEntry
  ---
  title: Développement web, besoins personnels
  startTime: 2020
  icon: panels-top-left
  ---
  **Développement d'une application** de gestion des Équipements de Protection Individuelle (frontend en vue et vuetify, backend en Ruby on Rails)
  ::

  ::TimelineEntry
  ---
  title: Administration système, DemoTic
  startTime: 2015
  endTime: 2016
  icon: square-terminal
  ---

  Bénévolat dans une boutique de services informatiques basés sur les **logiciels libres**.

  Mise en place du **réseau et outils internes**, conseil et dépannage
  ::

  ::TimelineEntry
  ---
  title: Administrateur système, Éducation Nationale
  startTime: 2012
  endTime: 2013
  icon: square-terminal
  ---

  Modernisation d'une infrastructure. Restructuration du **réseau interne**, déploiement d'un **cloud privé** (OpenNebula), **configuration centralisé** des serveurs (Chef Progress), mise à jour vers la dernière version stable de Debian, mise à jour d'une application pour compatibilité avec une version PHP plus récente.
  ::

  ::TimelineEntry
  ---
  title: Developpement, Freelance
  startTime: 2009
  endTime: 2012
  icon: panels-top-left
  ---

  **Administration serveur** Debian, corrections de bogues d'applications internes, développement d'une **application web** en Ruby on Rails, interfaçage entre applications internes et **applications libres** (Joomla, Dolibarr)
  ::

  ::TimelineEntry
  ---
  title: Développement, SmartJog
  startTime: 2008
  endTime: 2009
  last: true
  icon: square-terminal
  ---

  Développement divers sur les serveurs Debian installés chez les clients de la société : **outils techniques** en Python à destination du support, **logiciels métiers** avec **interface web**
  ::
::
