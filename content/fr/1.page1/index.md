::Page
#header
  ::PageHeader
  ---
  firstName: Jean-Philippe
  lastName: Garcia-Ballester
  title: Ingénieur IT
  ---
  Ma **personalité** est mon **atout** principal. J'**apprends** extrêmement vite et je peux facilement acquérir de **nouvelles compétences**.

  J'accorde beaucoup de valeur à la **satisfaction client** et je déploie une énergie considérable pour fournir un **travail de qualité**.

  Très **empathique**, je suis soucieux du bien-être de mes **collaborateurs** et priorise le **travail en équipe** sur mes intérêts personnels.
  ::
#default
  :ComponentContentDoc{path="/page1/main_experiences"}
#aside
  :AsideHeader
  :Avatar{avatar="/photo.webp"}
  ::InfoSection
  ---
  age: 38 ans
  phone: 06 31 78 40 48
  email: jip@rezal.cc
  address: Septmoncel, Jura
  openstreetmap: 291432218#map=14/46.3703/5.9120
  github: jip149
  gitlab: jip149
  ---
  ::
  ::ListSection{title="Logiciels"}
    ::ListEntry
    ---
    title: Langages
    items: ["Typescript", "Ruby", "Python", "PHP", "C", "C++"]
    ---
    ::
    
    ::ListEntry
    ---
    title: Web
    items: ["Vuejs", "vuetify", "TailwindCss", "DaisyUI", "RubyOnRails"]
    ---
    ::

    ::ListEntry
    ---
    title: Outils / systèmes
    items: ["git", "systemd", "Debian", "Ansible", "Docker", "Traefik"]
    ---
    ::
  ::
::
