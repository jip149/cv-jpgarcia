---
title: Compétences
component: SkillSection
---

::SkillEntry
---
  title: Capacité d'apprentissage
  value: 10
---
::
::SkillEntry
---
  title: Autonomie
  value: 7
---
::
::SkillEntry
---
  title: Travail en équipe
  value: 8
---
::
::SkillEntry
---
  title: Engagement
  value: 9
---
::
::SkillEntry
---
  title: Polyvalence
  value: 8
---
::
