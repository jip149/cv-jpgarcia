---
title: Formation
component: LayoutSection
---

::TimelineList
  ::TimelineEntry
  ---
  title: DEJEPS Moniteur de canyonisme
  startTime: 2019
  endTime: 2021
  first: true
  icon: mountain
  ---
  CREPS Vallon-Pont-d'Arc
  ::
  ::TimelineEntry
  ---
  title: Ingénierie informatique
  startTime: 2003
  endTime: 2008
  icon: laptop
  ---
  École Pour l'Informatique et les Techniques Avancées
  ::
  ::TimelineEntry
  ---
  title: Licence de Mathématiques Fondamentales
  startTime: 2006
  endTime: 2007
  last: true
  icon: graduation-cap
  ---
  Université Pierre et Marie Curie (Paris VI)
  ::
::
