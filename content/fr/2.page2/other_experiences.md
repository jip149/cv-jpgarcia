---
title: Autres expériences
component: LayoutSection
---

::TimelineList
  ::TimelineEntry{title="Bâtiment"}
  ---
  startTime: 2024
  endTime: Présent
  first: true
  icon: university
  ---

  Mission d'intérim en tant que charpentier-couvreur sur la restauration de **l'école de Nyon**, classée **monument historique**.
  ::

  ::TimelineEntry{title="Auto-construction d'une maison écologique"}
  ---
  startTime: 2024
  endTime: Présent
  first: true
  icon: home
  ---

  Auto-construction d'une maison **écologique, bio-climatique, passive**. Réalisation intégrale avec mon épouse.

  Accueil de bénévoles et **gestion de chantiers participatifs**.
  ::

  ::TimelineEntry{title="Travail social et éducation"}
  ---
  startTime: 2009
  endTime: 2021
  icon: users-round
  ---

  Nombreuses expériences dans le domaine du le travail social, avec différents publics.

  **Travail en équipe** pluri-disciplinaire
  ::

  ::TimelineEntry{title="Coopération et relations interpersonnelles"}
  ---
  startTime: 2009
  endTime: 2021
  icon: handshake
  ---

  Bénévole à EnVies EnJeux. Animation de jeux coopératifs, séjours de vacances pour enfants et adolescents. Nombreuses formations sur les **pratiques coopératives**, la **communication** et la **gestion de conflit**.
  :: 

  ::TimelineEntry{title="Gestion d'une association sportive"}
  ---
  startTime: 2011
  endTime: 2016
  last: true
  icon: mountain
  ---

  Président d'un club de montagne de 400 adhérents. **Coordination** des bénévoles, du salarié, et des prestataires, **organisation et animation des** réunions, vérification de **conformité des obligations légales** et **règles de sécurité**.
  :: 
::
