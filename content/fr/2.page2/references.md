---
title: Références
component: ReferenceSection
---
::ReferenceEntry
#default
  Jean-Philippe a une grande capacité technique, que ce soit par la « vision globale » d’un projet dans lequel il participe ou par la précision des solutions apportées aux tâches dont il a la charge.
#author
  J. Fernando Lagrange

  Ingénieur chez Probesys
::
