::Page
#header
  ::PageHeader
  ---
  firstName: Jean-Philippe
  lastName: Garcia-Ballester
  title: Linux System Administrator
  ---
  My **personality** is my greatest **strength**.

  I **learn** new things **extremely fast** and can acquire **new skills** very quickly.

  I value **customer satisfaction**, and can go to great length to ensure I do **quality work**

  Highly **empathetic**, I care about how **co-workers** think and feel, and prioritize **team work** over my own interest.
  ::
#default
  :ComponentContentDoc{path="/page1/main_experiences"}
#aside
  :AsideHeader
  :Avatar{avatar="/photo.webp"}
  ::InfoSection
  ---
  age: 38 years old
  permit: Work permit G
  phone: +41 76 222 90 28
  email: jip@rezal.cc
  address: Septmoncel, France
  openstreetmap: 291432218#map=14/46.3703/5.9120
  github: jip149
  gitlab: jip149
  ---
  ::
  ::ListSection{title="Software"}
    ::ListEntry
    ---
    title: Languages
    items: ["Typescript", "Ruby", "Python", "PHP", "C", "C++"]
    ---
    ::
    
    ::ListEntry
    ---
    title: Web
    items: ["Vuejs", "vuetify", "TailwindCss", "DaisyUI", "RubyOnRails"]
    ---
    ::

    ::ListEntry
    ---
    title: Tools / systems
    items: ["git", "systemd", "Debian", "Ansible", "Docker", "Traefik"]
    ---
    ::
  ::
::
