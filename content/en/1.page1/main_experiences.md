---
title: IT Experiences
component: LayoutSection
---

::TimelineList

  ::TimelineEntry
  ---
  title: Self-hosting, personal needs
  startTime: 2005
  endTime: Present
  first: true
  icon: square-terminal
  ---

  Self-hosting of my own infrastructure with **Podman/Docker** and **Ansible**.

  **Traefik**, **Knot** (DNS), **Stalwart** (SMTP/IMAP/JMAP), **Nextcloud** and others **PHP/RoR apps**
  ::

  ::TimelineEntry
  ---
  title: Web development, personal needs
  startTime: 2020
  icon: panels-top-left
  ---
  **Development of an app** for managing Personal Protective Equipment (vue and vuetify frontend, Ruby on Rails backend)
  ::

  ::TimelineEntry
  ---
  title: System Administration, DemoTic
  startTime: 2015
  endTime: 2016
  icon: square-terminal
  ---

  Volunteering in a computer shop providing free software related products and services, directed at the general public. **Network, servers, and internal tools** installation, customer service and troubleshooting
  ::

  ::TimelineEntry
  ---
  title: System Administration, Ministry of Education
  startTime: 2012
  endTime: 2013
  icon: square-terminal
  ---

  Infrastructure modernisation. **Internal network** Restructuration, deployment of a **private cloud** (OpenNebula), **centralized configuration** (Chef Progress), operating system upgrade (Debian latest stable version), legacy PHP application development for compatibility with recent PHP
  ::

  ::TimelineEntry
  ---
  title: Development, Freelance
  startTime: 2009
  endTime: 2012
  icon: panels-top-left
  ---

  **System Administration** on Debian servers, internal app bug fixes, web application development using Ruby on Rails, interface between internal apps and FOSS apps (Joomla, Dolibarr)
  ::

  ::TimelineEntry
  ---
  title: Development, SmartJog
  startTime: 2008
  endTime: 2009
  last: true
  icon: square-terminal
  ---

  Various development on Debian-based servers, deployed in customers' infrastructures: **technical CLI tools** in Python used by help desk staff, **web applications** for customers
  ::
::
