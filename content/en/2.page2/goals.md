---
title: Work Goals
component: SkillSection
---

::SkillEntry
---
  title: Solving technical problems
  value: 9
---
::
::SkillEntry
---
  title: Writing Quality code
  value: 10
---
::
::SkillEntry
---
  title: Contributing to FOSS
  value: 7
---
::
