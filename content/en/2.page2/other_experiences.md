---
title: Other experiences
component: LayoutSection
---

::TimelineList
  ::TimelineEntry{title="Construction"}
  ---
  startTime: 2024
  first: true
  icon: university
  ---

  Temporary work as roofer and carpenter on a historical monument, the elementary school of Nyon.
  ::
  ::TimelineEntry
  ---
  title: Environmentally-friendly house self-building
  startTime: 2021
  endTime: Present
  first: true
  icon: home
  ---

  Self-building of an **environmentally friendly, passive, bio-climatic** house. Full realization from the ground up. Hosting and **management of volunteering events**
  ::

  ::TimelineEntry
  ---
  title: Social work
  startTime: 2009
  endTime: 2021
  icon: users-round
  ---

  Several experiences in the field of social work and teaching, with handicapped people, children, people in position of social insecurity. **Multidisciplinary team-work**
  ::

  ::TimelineEntry
  ---
  title: Co-operation and interpersonal communication
  startTime: 2009
  endTime: 2021
  icon: handshake
  ---

  Volunteer at EnVies EnJeux, a French association. Coop games facilitation, summer camps for children. Multiple training courses about **co-operation**, **communication** and **conflict handling**
  :: 

  ::TimelineEntry
  ---
  Title: Leadership of a sports club
  startTime: 2010
  endTime: 2016
  last: true
  icon: mountain
  ---

  Leader of the climbing committee then president of a mountaineering club counting 400 members. Coordination of volunteers, employees, and contractors, meeting facilitation, control of **conformity with legal regulations** and **security rules**
  :: 
::
