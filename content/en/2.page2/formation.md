---
title: Education
component: LayoutSection
---

::TimelineList
  ::TimelineEntry
  ---
  title: Canyoning guide
  startTime: 2019
  endTime: 2021
  first: true
  icon: mountain
  ---
  DEJEPS CREPS Vallon-Pont-d'Arc
  ::
  ::TimelineEntry
  ---
  title: IT Engineer
  startTime: 2003
  endTime: 2008
  icon: laptop
  ---
  École Pour l'Informatique et les Techniques Avancées. Fourth and fifth years in the school research laboratory
  ::
  ::TimelineEntry
  ---
  title: Pure mathematics bachelor
  startTime: 2006
  endTime: 2007
  last: true
  icon: graduation-cap
  ---
  Pierre et Marie Curie (Paris VI) University
  ::
::
