---
title: Skills
component: SkillSection
---

::SkillEntry
---
  title: Learning
  value: 10
---
::
::SkillEntry
---
  title: Critical thinking
  value: 8
---
::
::SkillEntry
---
  title: Teamwork
  value: 8
---
::
::SkillEntry
---
  title: Dedication
  value: 9
---
::
::SkillEntry
---
  title: Versatility
  value: 8
---
::
