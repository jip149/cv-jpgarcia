::Page
#header

  ::PageHeaderSmall
  ---
  firstName: Jean-Philippe
  lastName: Garcia-Ballester
  title: IT Engineer
  ---
  ::

#default
  :ComponentContentDoc{path="/page2/formation"}
  :ComponentContentDoc{path="/page2/other_experiences"}
  :ComponentContentDoc{path="/page2/references"}
#aside
  :ComponentContentDoc{path="/page2/skills"}
  :ComponentContentDoc{path="/page2/goals"}
  :ComponentContentDoc{path="/page2/languages"}
  ::MadeWith
    Made with :Icon{name="heart" fill="info" stroke="info" :inline="true"} using
    * [Nuxt Content](https://content.nuxt.com/){color="neutral"}
    * [Tailwind CSS](https://tailwindcss.com){color="neutral"}
    * [DaisyUI](https://daisyui.com){color="neutral"}

    Source on [gitlab](https://gitlab.com/jip149/cv-jpgarcia){color="neutral"}
  ::
::
