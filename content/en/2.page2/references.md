---
title: Reference
component: ReferenceSection
---
::ReferenceEntry
#default
  Jean-Philippe has great technical skills, with both a global vision of a project he's involved in and precision in the solutions he comes up with to the problems he's in charge of.
#author
  J. Fernando Lagrange

  SysAdming Engineer at Probesys
::
