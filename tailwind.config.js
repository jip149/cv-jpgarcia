/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  theme: {
    extend: {},
  },
  plugins: [require('daisyui'),],
  
  daisyui: {
    themes: [
      "nord",
    ],
  },

   safelist: [
    'fill',
    {
      pattern: /(fill|stroke)-(primary|secondary|accent|info)/,
    },
    'link',
    {
      pattern: /(link)-(primary|neutral)/,
    },
  ]
}

